<?php
/**
 * Wrapper function for modals class
 */
function du_modal( $template ) {
  $du_modal = new du\modals( $template, true );
  $du_modal->direct_window();
  echo 'modal-trigger="' . $template . '"';
}

/**
 * Return modal slug (id) based on modal template filename
 */
function du_modal_id( $file_name ) {
  $modal_id = str_replace( 'modal-', '', basename( $file_name, '.php' ) );
  return $modal_id;
}