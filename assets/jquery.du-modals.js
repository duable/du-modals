/**
  * Du Minimal Modals
  *
  * Very, very minimal modals
  * [modal-status="hide||show"]
  * [modal-id="slug"]
  * [modal-close] main modal close button
  * [modal-force-close] closes modal window
  */

// Modal triggering
jQuery( 'html' ).on( 'click', '[modal-trigger]', function() {
  var modal = '[modal-id="' + jQuery( this ).attr( 'modal-trigger' ) + '"]';
  du_open_modal( modal );
});

// Close modal buttons
jQuery( 'html' ).on( 'click', '[modal-close], [modal-force-close], [modal-overlay]', function() {
  du_close_modals();
});

// Insert overlay layer
jQuery( 'body' ).append( '<aside class="overlay" modal-overlay></aside>' );

document.onkeydown = function(evt) {
  evt = evt || window.event;
  if (evt.keyCode == 27 && jQuery( 'body.du-modal' ).length > 0 ) {
    du_close_modals();
  }
};

function du_open_modal( modal ){
  // Store current window position
  jQuery( 'body' ).attr( 'data-du-body-scroll', jQuery( window ).scrollTop() );
  // Use these values to disable scrolling while nav is visible
  var scroll = Number( -Math.abs( jQuery( 'body' ).attr( 'data-du-body-scroll' ) ) );
  var padding = Number( Math.abs( jQuery( 'body' ).attr( 'data-du-body-scroll' ) ) );
  jQuery( 'body' ).addClass( 'no-scroll du-modal' );
  jQuery( 'body' ).css( 'top', scroll );
  jQuery( 'body' ).css( 'padding-bottom', padding + 'px');
  jQuery( 'body' ).attr( 'du-modal-visible', modal );
  jQuery( modal ).attr( 'modal-status', 'show' );
  jQuery( 'body' ).append( '<a modal-close>close</a>' );

  /** 
   * Select2 Dropdown Position Fix 
   */
  jQuery( 'body' ).append( '<aside class="select2_styles"></aside>' );
  jQuery( 'aside.select2_styles' ).html( '<style>.select2-dropdown{top:' + scroll + 'px;}' );
}


function du_close_modals(){
  var scroll = jQuery( 'body' ).attr( 'data-du-body-scroll' );

  // Reset padding and top values
  jQuery( 'body' ).css( 'padding-bottom', 0 );
  jQuery( 'body' ).css( 'top', 'auto' );

  // Unlock scrolling
  jQuery( 'body' ).removeClass( 'no-scroll du-modal' );
  jQuery( 'body' ).removeAttr( 'du-modal-visible' );
  jQuery( '[modal-status]' ).attr( 'modal-status', 'hide' );
  jQuery( '[modal-close]' ).remove();


  // Scroll to original location
  jQuery( document ).scrollTop( scroll );

  // Clear values
  jQuery( 'body' ).attr( 'data-du-body-scroll', 0 );

  /**
   * Stop all videos that are playing
   */
  jQuery( '[modal-status] iframe' ).each( function() { 
    var src = jQuery( this ).attr( 'src' );
    jQuery( this ).attr( 'src', src );  
  });

  /** 
   * Select2 Dropdown Position Fix 
   */
  jQuery( 'aside' ).remove( '.select2_styles' );
}