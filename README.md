# Usage

1. Create a modal-{slug}.php file in /modals/ directorty.  Use modal-default.php as a template.
2. To create a link that opens the modal, <a <?php du_modal( '{slug}' ); ?>>Open Modal</a>

## Options 

On the <aside> that houses the modal, you can set modal-position="left" to either right/top/bottom/left.

