<?php

namespace du;

# Include wrapper functions
include_once( __DIR__ . '/functions.php' );

/**
 * Minimal Modals
 * 
 * @filter du_modal_js_script  path to modal js relative to theme dir
 * @filter du_modal_css_script  path to modal css relative to theme dir
 * @package default
 * @author dev@duable.com
 **/
class modals {

  function __construct( $template = null, $second_run = false ) {
    add_filter( 'du_js_files', array( $this, 'load_js' ) );
    add_filter( 'du_css_files', array( $this, 'load_css' ) );
    /**
     * If a template is specified, then let's set the template variable
     * and NOT add the action to wp_footer since its a one-off.
     */
    if ( !empty( $template ) ) 
      $this->template = $template;
    //add_action( 'wp_footer', array( $this, 'windows' ), 999 );

    # Check if modal should be opened by form field
    if ( !$second_run ) {
      if ( !empty( $_POST[ 'du_open_modal' ] ) ) :
        $modal_window = $_POST[ 'du_open_modal' ];
        $this->modal_window = $modal_window;
        add_action( 'wp_footer', array( $this, 'open_modal_callback' ) );
      endif;
    }

    if ( !empty( $_GET[ 'du_open_modal' ] ) ) :
      $this->modal_window = $_GET[ 'du_open_modal' ];
      $this->direct_window();
      add_action( 'wp_footer', array( $this, 'open_modal_callback' ) );
    endif;
  }

  function load_js( $js_files ) {
    $modal_js = "/vendor/duable/du-modals/assets/jquery.du-modals.js";
    $modal_js = apply_filters( 'du_modal_js_script', $modal_js );
    if ( $js_files[ 'polyfills' ] ) :
      $js_files = array( 'polyfills' => $js_files[ 'polyfills' ], 'modals' => $modal_js ) + $js_files;
    else :
      $js_files = array( 'modals' => $modal_js ) + $js_files;
    endif;
    return $js_files;
  }

  function load_css( $css_files ) {
    $modal_css = "/vendor/duable/du-modals/assets/du-modals.css";
    $modal_css = apply_filters( 'du_modal_css_script', $modal_css );

    $css_files = array( 'modals' => $modal_css ) + $css_files;
    return $css_files;
  }

  public function direct_window() {
    add_action( 'wp_footer', array( $this, 'get_template' ) );
  }
  
  public function get_template() {
    if ( !empty( $this->template ) ) :
      echo "\n";
      echo '<!-- ' . $this->template . ' modal window -->' . "\n"; 
      $template_file = get_stylesheet_directory() . '/modals/modal-' . $this->template . '.php';
      echo "\n";
    else :
      echo "\n";
      echo '<!-- ' . $this->modal_window . ' modal window -->' . "\n"; 
      $template_file = get_stylesheet_directory() . '/modals/modal-' . $this->modal_window . '.php';
      echo "\n";
    endif;
    
    if ( file_exists( $template_file ) )
      include_once( $template_file );
  }

  function open_modal_callback() {
    echo "\n";
    echo '<!-- Open modal window on page load. -->' . "\n";
    echo '<script>jQuery( window ).load( function() { du_open_modal( "[modal-id=\"' . $this->modal_window . '\"]" ); } );</script>' . "\n";
    echo "\n";
  }

} new modals;